package com.example.capsula02.pruebafirebase;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;

public class MainActivity extends AppCompatActivity {

    EditText editText;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FirebaseMessaging.getInstance().subscribeToTopic("test");
        FirebaseInstanceId.getInstance().getToken();

        String token = FirebaseInstanceId.getInstance().getToken();
        Log.i("tokenMain",""+token);
        RegisterToken(token);

        editText = findViewById(R.id.token);
        editText.setText(token);
    }

    private void RegisterToken(String refreshedToken) {
        Log.i("token",""+refreshedToken);
        String URL   = "http://rockenwebperu.com/public_pruebafirebase/public/user/token?user_token="+refreshedToken;
        Log.i("url",URL);
        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest  = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.i("errorrespuesta","error");

            }
        });

        queue.add(stringRequest);

    }
}
